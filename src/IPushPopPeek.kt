interface IPushPopPeek<T>
{
    fun push(el: T) : Boolean
    fun pop() : T
    fun peek(): T
    val isEmpty : Boolean
}