package dataStructure

import dataStructure.GraphType.*

//todo: Раскидать по файликам

interface graph<T> {
  fun connect(source: T, destination: T): Graph<T>
}
enum class GraphType {
    Undirected, Directed, Mixed, Weighted, Unweighted
}

class DirectedGraph<T> : graph<T>, Graph<T>() {
    override fun connect(source: T, destination: T): Graph<T> {
        //Todo: exception, если пытаемся создать связь, которая из-за которой, граф перестанет быть направленным
        return oneSideConnect(source, destination)
    }
}

open class Graph<T> : graph<T>{
    protected data class Vertex<T>(val sourceVertex: T) {
        val edges = mutableListOf<Edge<T>>()

        //todo по играться с lazy
//        val edges : MutableList<Edge<T>> by lazy { mutableListOf<Edge<T>>() }

        //val edges = mutableSetOf<Vertex<T>>()
    }

    //сравнение объектов, при добавление новых полей, надо перегрузить equal
    protected data class Edge<T>(val destinationVertex: Vertex<T>)

    private val vertexes = mutableListOf<Vertex<T>>()

    //возможна циклическая ссылка
    private fun connect(sourceVertex: Vertex<T>?, destinationVertex: Vertex<T>?): Graph<T> {
        if (sourceVertex != null && destinationVertex != null) {
            val edgeTo = Edge(destinationVertex)
            val edgeFrom = Edge(sourceVertex)
            if (!sourceVertex.edges.contains(edgeTo)) {
                sourceVertex.edges.add(edgeTo)
            }
            if (!destinationVertex.edges.contains(edgeFrom)) {
                destinationVertex.edges.add(edgeFrom)
            }
        }
        return this
    }

    private fun oneSideConnect(sourceVertex: Vertex<T>?, destinationVertex: Vertex<T>?): Graph<T> {
        if (sourceVertex != null && destinationVertex != null) {
            val edgeTo = Edge(destinationVertex)
            if (!sourceVertex.edges.contains(edgeTo)) {
                sourceVertex.edges.add(Edge(destinationVertex))
            }
        }
        return this
    }

    private fun get(key: T) = vertexes.singleOrNull { it.sourceVertex == key }

    fun size() = vertexes.size

    fun addVertex(obj: T) {
        if (obj != null && !hasVertex(obj)) {
            vertexes.add(Vertex(obj))
        }
    }

    fun hasVertex(v: T) = vertexes.any { it.sourceVertex != null && it.sourceVertex == v }

    override fun connect(source: T, destination: T): Graph<T> = connect(get(source), get(destination))

    open fun oneSideConnect(source: T, destination: T): Graph<T> = oneSideConnect(get(source), get(destination))

    fun hasConnect(source: T, destination: T) = neighbors(source).contains(destination)

    fun hasBiConnect(source: T, destination: T) =
        neighbors(source).contains(destination) && neighbors(destination).contains(source)

    fun neighbors(key: T): List<T> = get(key)?.edges?.map { it.destinationVertex.sourceVertex } ?: listOf()

    fun getGraphType(): GraphType {
        return if (isDirectedGraph()) Directed else Undirected
    }

    private fun isDirectedGraph(): Boolean {
        val q = Queue<Vertex<T>>()
        q.push(vertexes)

        while (!q.isEmpty) {
            val v = q.pop()
            val adj = neighbors(v.sourceVertex)

            for (a in adj) {
                if (hasBiConnect(v.sourceVertex, a)) return false
            }
        }
        return true
    }

    companion object {
        fun <T> createGraph(graph: Graph<T>):graph<T> = when (graph.getGraphType()) {
            Directed -> DirectedGraph()
            Undirected -> Graph()

            Mixed -> TODO()
            Weighted -> TODO()
            Unweighted -> TODO()
        }

    }

}



























