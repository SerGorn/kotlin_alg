package dataStructure

import IPushPopPeek
import UnderflowException

class Queue<T>: IPushPopPeek<T>
{
    private val _items = mutableListOf<T>()

    override fun push(el: T): Boolean = _items.add(el)

    override fun pop(): T {
        val res = peek()
        _items.remove(res)
        return res
    }

    override fun peek(): T = if(isEmpty) throw UnderflowException("Попытка получения элемента(peek) из очереди в которой нет элементов.") else _items.first()

    fun size() = _items.size

    override val isEmpty: Boolean
        get() = _items.size == 0
}

fun <T> Queue<T>.push(items: Collection<T>) = items.forEach { this.push(it) }
fun <T> Queue<T>.push(items: Array<T>?) = items?.forEach { this.push(it) } ?: throw NullPointerException()
