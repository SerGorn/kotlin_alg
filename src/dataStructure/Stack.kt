package dataStructure

import IPushPopPeek
import UnderflowException

class Stack<T> : IPushPopPeek<T> {
    private val _items = mutableListOf<T>()

    override fun push(el: T) = _items.add(el)

    override fun pop(): T {
        val res = this.peek()
        _items.remove(res)
        return res
    }

    override fun peek(): T =
        if (isEmpty) throw UnderflowException("Попытка получения элемента(peek) из стека в котором нет элементов.") else _items.last()

    override val isEmpty: Boolean
        get() = this.count == 0

    val count: Int
        get() = _items.size

}
