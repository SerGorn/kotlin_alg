package algorithm

import dataStructure.DirectedGraph
import dataStructure.Queue
import dataStructure.push

fun <T> DirectedGraph<T>.breadthFirstSearch(startNode: T, equalCond: (T) -> Boolean ) : T?
{
    val searchQueue = Queue<T>()
    val searched = listOf<T>()
    searchQueue.push(this.neighbors(startNode))

    while (!searchQueue.isEmpty) {
        val p = searchQueue.pop()
        if (!searched.contains(p)) {
            if (equalCond(p)) {
                return p
            } else {
                searchQueue.push(this.neighbors(p))
                searched.plus(p)
            }
        }
    }
    return null
}
