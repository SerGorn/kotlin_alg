package dataStructureTest

import UnderflowException
import dataStructure.Stack
import org.junit.jupiter.api.Test
import kotlin.test.*

internal class StackTest {

    @Test
    fun push() {
        val s = Stack<Int>()
        s.push(8)
        assertEquals(8, s.peek())
    }

    @Test
    fun pop() {
        val s = Stack<Int>()
        assertFailsWith(UnderflowException::class) { s.pop() }
        s.push(2)
        s.push(4)
        s.push(6)
        s.push(8)
        assertEquals(8, s.pop())
        assertEquals(3, s.count)
    }

    @Test
    fun peek() {
        val s = Stack<Int>()
        assertFailsWith(UnderflowException::class) { s.peek()  }
        s.push(2)
        s.push(4)
        s.push(6)
        s.push(8)
        assertEquals(8, s.peek())
        assertEquals(4, s.count)
    }

    @Test
    fun getCount() {
        val s = Stack<Int>()
        s.push(1)
        s.push(1)
        s.push(1)
        assertEquals(3, s.count)
    }
}