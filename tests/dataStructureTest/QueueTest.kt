package dataStructureTest

import UnderflowException
import dataStructure.Queue
import dataStructure.push
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue


internal class QueueTest {

    @Test
    fun push() {
        val q = Queue<Int>()
        q.push(1)
        assertEquals(1, q.peek())

        q.push(2)
        assertEquals(1, q.peek())
    }

    @Test
    fun pushArray() {
        val q = Queue<Int>()
        val array = arrayOf(1,2,3,4,5)
        q.push(array)
        assertEquals(5, q.size())
        assertEquals(1, q.pop())
        assertEquals(2, q.peek())

        val array2: Array<Int>? = null

        assertFailsWith(NullPointerException::class) { q.push(array2) }
    }

    @Test
    fun pushCollection() {
        val q = Queue<Int>()
        val array = arrayListOf(1,2,3,4,5)
        q.push(array)
        assertEquals(5, q.size())
        assertEquals(1, q.pop())
        assertEquals(2, q.peek())
    }

    @Test
    fun pop() {
        val q = Queue<Int>()
        assertFailsWith(UnderflowException::class) { q.pop() }
        q.push(1)
        assertEquals(1, q.peek())
        q.push(2)
        assertEquals(1, q.peek())
    }

    @Test
    fun peek() {
        val q = Queue<Int>()
        assertFailsWith(UnderflowException::class) { q.peek() }
        q.push(1)
        q.push(2)
        assertEquals(1, q.peek())
    }

    @Test
    fun isEmpty() {
        val q = Queue<Int>()
        q.push(1)
        q.push(2)
        q.push(3)
        q.push(4)
        q.pop()
        q.pop()
        q.pop()
        q.pop()
        assertTrue(q.isEmpty)
    }
}