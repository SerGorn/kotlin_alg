package dataStructureTest

import dataStructure.DirectedGraph
import dataStructure.Graph
import dataStructure.GraphType
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class GraphFactoryTest {

    val alice = GraphTest.Person("Alice", false)
    val bob = GraphTest.Person("bob", false)
    val claire = GraphTest.Person("claire", false)

    @Test
    fun create() {
        val graph = Graph<GraphTest.Person>()
        graph.addVertex(alice)
        graph.addVertex(bob)
        graph.addVertex(claire)

        graph.oneSideConnect(alice, bob)
        graph.oneSideConnect(bob, claire)
        graph.oneSideConnect(claire, alice)

        assertTrue(Graph.createGraph(graph) is DirectedGraph<GraphTest.Person>)

        graph.oneSideConnect(bob, alice)

        assertTrue(Graph.createGraph(graph) is Graph<GraphTest.Person>)
    }
}