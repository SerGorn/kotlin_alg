package dataStructureTest

import dataStructure.Graph
import dataStructure.GraphType
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue


internal class GraphTest {

    data class Person(val Name: String, val IsSeller: Boolean)

    val alice = Person("Alice", false)
    val bob = Person("bob", false)
    val claire = Person("claire", false)
    val penny = Person("penny", false)

    @Test
    fun size() {
        val graph = Graph<Person>()
        assertEquals(0, graph.size())
        graph.addVertex(alice)
        assertEquals(1, graph.size())
        graph.addVertex(alice)
        assertEquals(1, graph.size())
        graph.addVertex(bob)
        assertEquals(2, graph.size())
    }

    @Test
    fun hasConnect() {
        val graph = Graph<Person>()
        graph.addVertex(alice)
        graph.addVertex(bob)

        graph.connect(alice, bob)

        assertTrue(graph.hasConnect(alice, bob))
        assertTrue(graph.hasConnect(bob, alice))
    }

    @Test
    fun addVertex() {
        val graph = Graph<Person>()
        assertFalse(graph.hasVertex(alice))

        graph.addVertex(alice)
        assertTrue(graph.hasVertex(alice))
    }

    @Test
    fun addVertex_null() {
        val graph = Graph<String?>()

        val str = null
        graph.addVertex(str)
        assertEquals(0, graph.size())
    }

    @Test
    fun connect() {
        val graph = Graph<Person>()
        graph.addVertex(alice)
        graph.addVertex(bob)

        assertEquals(0, graph.neighbors(alice).size)

        graph.connect(alice, bob)

        assertEquals(1, graph.neighbors(alice).size)
        assertEquals(bob, graph.neighbors(alice).first())

        assertEquals(1, graph.neighbors(bob).size)
        assertEquals(alice, graph.neighbors(bob).first())

        graph.connect(bob, alice)

        assertEquals(1, graph.neighbors(alice).size)
        assertEquals(1, graph.neighbors(bob).size)
        assertEquals(bob, graph.neighbors(alice).first())
        assertEquals(alice, graph.neighbors(bob).first())


    }

    @Test
    fun oneSideConnect() {
        val graph = Graph<Person>()
        graph.addVertex(alice)
        graph.addVertex(bob)

        assertEquals(0, graph.neighbors(alice).size)

        graph.oneSideConnect(alice, bob)

        assertTrue(graph.hasConnect(alice,bob))
        assertFalse(graph.hasConnect(bob,alice))

        assertFalse(graph.hasBiConnect(alice,bob))

        assertEquals(1, graph.neighbors(alice).size)
        assertEquals(bob, graph.neighbors(alice).first())

        assertEquals(0, graph.neighbors(bob).size)
    }

    @Test
    fun hasBiConnect() {
        val graph = Graph<Person>()
        graph.addVertex(alice)
        graph.addVertex(bob)

        assertFalse(graph.hasBiConnect(alice,bob))

        graph.connect(alice, bob)

        assertTrue(graph.hasBiConnect(alice,bob))
        assertTrue(graph.hasBiConnect(bob,alice))

    }

    @Test
    fun hasBiConnect_2() {
        val graph = Graph<Person>()
        graph.addVertex(alice)
        graph.addVertex(bob)

        graph.oneSideConnect(alice, bob)

        assertFalse(graph.hasBiConnect(alice,bob))
        assertFalse(graph.hasBiConnect(bob,alice))

        graph.oneSideConnect(bob, alice)

        assertTrue(graph.hasBiConnect(alice,bob))
        assertTrue(graph.hasBiConnect(bob,alice))

    }

    @Test
    fun neighbors() {
        val graph = Graph<Person>()
        graph.addVertex(alice)
        graph.addVertex(bob)
        graph.addVertex(claire)
        graph.addVertex(penny)

        graph.connect(alice, bob)
        graph.connect(alice, claire)
        graph.connect(alice, penny)

        graph.connect(bob, alice)
        graph.connect(bob, claire)

        assertEquals(3, graph.neighbors(alice).size)
        assertEquals(1, graph.neighbors(penny).size)
    }

    @Test
    fun hasVertex() {
        val graph = Graph<Person>()
        assert(!graph.hasVertex(alice))
        graph.addVertex(bob)
        assert(!graph.hasVertex(alice))
        assert(graph.hasVertex(bob))
    }

    @Test
    fun isDirectedGraph_1() {
        val graph = Graph<Person>()

        graph.addVertex(alice)
        graph.addVertex(bob)
        graph.addVertex(claire)

        assertEquals(graph.getGraphType(), GraphType.Directed,  "Является пустым направленным ациклический(гамак) графом")
    }

    @Test
    fun isDirectedGraph_2() {
        val graph = Graph<Person>()
        graph.addVertex(alice)
        graph.addVertex(bob)
        graph.addVertex(claire)

        graph.oneSideConnect(alice, bob)
        assertEquals(graph.getGraphType(), GraphType.Directed,  "Является направленным ациклический(гамак) графом")
    }

    @Test
    fun isDirectedGraph_3() {
        val graph = Graph<Person>()
        graph.addVertex(alice)
        graph.addVertex(bob)
        graph.addVertex(claire)

        graph.oneSideConnect(alice, bob)

        graph.oneSideConnect(bob, claire)
        assertEquals(graph.getGraphType(), GraphType.Directed,  "Является направленным односторонним ациклический(гамак) графом")

    }

    @Test
    fun isDirectedGraph_4() {
        val graph = Graph<Person>()
        graph.addVertex(alice)
        graph.addVertex(bob)
        graph.addVertex(claire)

        graph.oneSideConnect(alice, bob)

        graph.oneSideConnect(bob, claire)

        graph.oneSideConnect(claire, alice)
        assertEquals(graph.getGraphType(), GraphType.Directed, "Является сильным направленным турниром")

    }

    @Test
    fun isDirectedGraph_5() {
        val graph = Graph<Person>()
        graph.addVertex(alice)
        graph.addVertex(bob)
        graph.addVertex(claire)

        graph.oneSideConnect(alice, bob)
        graph.oneSideConnect(bob, claire)
        graph.oneSideConnect(claire, alice)
        graph.oneSideConnect(bob, alice)

        assertEquals(graph.getGraphType(), GraphType.Undirected, "Является односторонним графом")
    }
}