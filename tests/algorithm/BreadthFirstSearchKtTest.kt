package algorithm

import dataStructure.DirectedGraph
import org.junit.jupiter.api.Test
import kotlin.test.*


internal class BreadthFirstSearchKtTest {

    data class Person(val Name: String, val IsSeller: Boolean)

    val alice = Person("Alice", false)
    val bob = Person("bob", false)
    val claire = Person("claire", false)
    val penny = Person("penny", false)
    val peggy = Person("peggy", false)
    val thom = Person("thom", false)
    val you = Person("you", false)
    val johnny = Person("johnny", false)
    val anuj = Person("anuj", false)

    val gwen = Person("gwen", false)
    val gloria = Person("gloria", true)



    @Test
    fun breadthFirstSearch() {
        val graph =  DirectedGraph<Person>()
        graph.addVertex(alice)
        graph.addVertex(bob)
        graph.addVertex(claire)
        graph.addVertex(penny)
        graph.addVertex(peggy)
        graph.addVertex(thom)
        graph.addVertex(johnny)
        graph.addVertex(you)
        graph.addVertex(anuj)

        graph.connect(you,alice).connect(you,bob).connect(you,claire)
        graph.connect(bob,anuj).connect(bob,peggy)
        graph.connect(alice,peggy)
        graph.connect(claire,thom).connect(claire,johnny)

        val r = graph.breadthFirstSearch(you){it -> it.IsSeller}

        assertNull(r)

        graph.addVertex(gwen)
        graph.addVertex(gloria)
        graph.connect(gwen,gloria)

        val r2 = graph.breadthFirstSearch(you){it -> it.IsSeller}
        assertNull(r2)

        graph.connect(anuj,gwen)

        val r3 = graph.breadthFirstSearch(you){it -> it.IsSeller}
        assertEquals(r3, gloria)
    }



}